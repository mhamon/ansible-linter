FROM docker.io/alpine:3 as builder
ARG ANSIBLE_LINT_VERSION

RUN set -eux && \
    apk update && \
    apk add --no-cache bc cargo gcc git libffi-dev musl-dev openssl-dev py3-pip python3 python3-dev rust
RUN set -eux && \
    if [ "${ANSIBLE_LINT_VERSION}" = "latest" ]; then \
        pip3 install --no-cache-dir --no-compile ansible-lint ;\
	else \
		pip3 install --no-cache-dir --no-compile "ansible-lint>=${ANSIBLE_LINT_VERSION},<$(echo "${ANSIBLE_LINT_VERSION}+1" | bc)"; \
	fi && \
	pip3 install --no-cache-dir --no-compile ansible yamllint && \
	ansible-lint --version && \
	yamllint --version && \
	find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf && \
	find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

FROM docker.io/alpine:3 as worker
ARG ANSIBLE_LINT_VERSION
ARG IMAGE_CREATED IMAGE_VERSION IMAGE_REVISION IMAGE_AUTHOR IMAGE_SOURCE

# https://github.com/opencontainers/image-spec/blob/master/annotations.md
LABEL "org.opencontainers.image.created"="$IMAGE_CREATED"
LABEL "org.opencontainers.image.version"="$IMAGE_VERSION"
LABEL "org.opencontainers.image.revision"="$IMAGE_REVISION"
LABEL "maintainer"="$IMAGE_AUTHOR"
LABEL "org.opencontainers.image.authors"="$IMAGE_AUTHOR"
LABEL "org.opencontainers.image.vendor"="Homelab"
LABEL "org.opencontainers.image.licenses"="WTFPL"
LABEL "org.opencontainers.image.url"="$IMAGE_SOURCE"
LABEL "org.opencontainers.image.documentation"="$IMAGE_SOURCE"
LABEL "org.opencontainers.image.source"="$IMAGE_SOURCE"
LABEL "org.opencontainers.image.ref.name"="ansible-lint ${ANSIBLE_LINT_VERSION}"
LABEL "org.opencontainers.image.title"="ansible-lint ${ANSIBLE_LINT_VERSION}"
LABEL "org.opencontainers.image.description"="ansible-lint ${ANSIBLE_LINT_VERSION}"

RUN set -eux && \
    apk update && apk add --no-cache bash git python3 && \
	find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf && \
	find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf && \
	mv /usr/lib/python$(python3 -V | grep -Eo '[0-9]\.[0-9]+') /usr/lib/python3 && \
	ln -s /usr/lib/python3 /usr/lib/python$(python3 -V | grep -Eo '[0-9]\.[0-9]+')

COPY --from=builder /usr/lib/python3.11/site-packages/ /usr/lib/python3/site-packages/
COPY --from=builder /usr/bin/ansible-lint /usr/bin/ansible-lint
COPY --from=builder /usr/bin/ansible /usr/bin/ansible
COPY --from=builder /usr/bin/ansible-config /usr/bin/ansible-config
COPY --from=builder /usr/bin/ansible-connection /usr/bin/ansible-connection
COPY --from=builder /usr/bin/ansible-doc /usr/bin/ansible-doc
COPY --from=builder /usr/bin/ansible-galaxy /usr/bin/ansible-galaxy
COPY --from=builder /usr/bin/ansible-inventory /usr/bin/ansible-inventory
COPY --from=builder /usr/bin/ansible-playbook /usr/bin/ansible-playbook
COPY --from=builder /usr/bin/ansible-vault /usr/bin/ansible-vault
COPY --from=builder /usr/bin/yamllint /usr/bin/yamllint

RUN set -eux && \
	ansible-lint --version && \
	yamllint --version && \
	find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf && \
	find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

RUN set -eux && \
	echo -en "[safe]\n    directory = /data\n" >> /root/.gitconfig

WORKDIR /data
ENTRYPOINT ["ansible-lint"]
CMD ["--version"]
